from django.contrib.auth.models import User
from accounts.models import Profile

#This allows templates to access a user's profile (specifically for the base template)
def user_get_santaclara_profile(self):
	profile = Profile.objects.get(user=self)
	return profile

User.add_to_class('get_santaclara_profile', user_get_santaclara_profile)
