# Authorises the app with Facebook and stores the resulting account details
from django.http import HttpResponseRedirect
from accounts.models import *
from api_keys import FACEBOOK_APP_ID, FACEBOOK_APP_SECRET
from santaclara.urls import FACEBOOK_AUTH_REDIRECT_URL

from oauth2client.client import OAuth2WebServerFlow
import urllib
import json

def get_authorise_url(request):
    #args = dict(client_id=FACEBOOK_APP_ID, redirect_uri=AUTH_REDIRECT_URL)
    flow_obj = OAuth2WebServerFlow(auth_uri="https://graph.facebook.com/oauth/authorize?",
                            client_id=FACEBOOK_APP_ID,
                            client_secret=FACEBOOK_APP_SECRET,
                            scope="xmpp_login",
                            redirect_uri=FACEBOOK_AUTH_REDIRECT_URL)

    auth_uri = flow_obj.step1_get_authorize_url()
    # Store flow in db for oauth2callback
    flow_field = Flow(user=request.user, service=Account.FACEBOOK, flow=flow_obj)
    flow_field.save()

    return HttpResponseRedirect(auth_uri)


