from django.http import HttpResponseRedirect
from accounts.models import *
from api_keys import GOOGLE_APP_ID, GOOGLE_APP_SECRET
from santaclara.urls import GOOGLE_AUTH_REDIRECT_URL

from oauth2client.client import OAuth2WebServerFlow
import urllib2
import json

def get_authorise_url(request):
	#authorise a google account (put this in /accounts/google.py)
    flow_obj = OAuth2WebServerFlow(client_id=GOOGLE_APP_ID,
                            client_secret=GOOGLE_APP_SECRET,
                            scope="https://mail.google.com/ https://www.googleapis.com/auth/userinfo.email",
                            redirect_uri=GOOGLE_AUTH_REDIRECT_URL)

    auth_uri = flow_obj.step1_get_authorize_url()

    # Store flow in db for oauth2callback
    flow_field = Flow(user=request.user, service=Account.GOOGLE, flow=flow_obj)
    flow_field.save()

    return HttpResponseRedirect(auth_uri)

