from django.db import models
from django.conf import settings
from main.models import *
from oauth2client.django_orm import CredentialsField, FlowField
from django.utils.translation import ugettext_lazy as _

# Relevant for all Santaclara registered users
# If user isn't registered in Santaclara, the messages.sender
# class is used instead of this
class Profile(models.Model):
    user = models.OneToOneField(User)
    forename = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)
    profile_pic = models.FileField(upload_to='profile_pics',
                                   default=settings.STATIC_URL + 'images/profile_default.jpg')
    bio = models.TextField(blank=True)
    status = models.TextField(blank=True)

    class Meta:
        verbose_name = _('profile')
        verbose_name_plural = _('profiles')

    def __unicode__(self):
        return self.forename + " " + self.surname


#Base account describing each account.
#This information is expanded by a detailed "[service]Account"
class Account(models.Model): 
    SANTACLARA = 'sc'
    GOOGLE = 'gl'
    FACEBOOK = 'fb'
    #YAHOO = 'yh'
    #etc

    #**NOTE: don't use the values here: these are for reference by the Model Field.
    #        Instead use the variables above**
    SERVICE = (
        (GOOGLE, 'google' ),
        (FACEBOOK, 'facebook')
        #(YAHOO, 'yahoo')
    )

    user = models.ForeignKey(User)
    service = models.CharField(max_length=2,
                                choices=SERVICE,
                                default=SANTACLARA)
    username = models.TextField()
    time_created = models.DateTimeField(auto_now_add=True)
    time_updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('account')
        verbose_name_plural = _('accounts')

    def __unicode__(self):
        return self.service + ": " + self.username

class GoogleAccount(models.Model):
    base_account = models.OneToOneField(Account)
    email_address = models.EmailField(max_length=254)
    credentials = CredentialsField()

    def __unicode__(self):
        return self.email_address

    class Meta:
        verbose_name = _('Google account')
        verbose_name_plural = _('Google accounts')

class Flow(models.Model):
    user = models.OneToOneField(User, related_name="user")
    service = models.CharField(max_length=2,
                                choices=Account.SERVICE,
                                default=Account.SANTACLARA)
    flow = FlowField()

    def __unicode__(self):
        return self.user.username()
