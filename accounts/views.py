from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.core.exceptions import ValidationError
from django.shortcuts import render,get_object_or_404
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect
from django import forms
from accounts.models import Profile, Flow, Account, GoogleAccount

import urllib2
import json
import datetime
import hashlib

from accounts.google import get_authorise_url as auth_google
from accounts.facebook import get_authorise_url as auth_facebook
from accounts.models import *

class SignUpForm(forms.Form):
    username = forms.RegexField(label=_("Username"),
                                min_length=3,
                                max_length=100,
                                regex=r'^\w+$')
    password = forms.CharField(label=_("Choose a Password"),
                               min_length=5,max_length=128,
                               widget=forms.PasswordInput)
    password_check = forms.CharField(label=_("Confirm Password"),
                                     min_length=5,
                                     max_length=128,
                                     widget=forms.PasswordInput)
    accept_tos = forms.BooleanField(label=("You accept the ToS"),
                                    required=True)
    accept_tos.error_messages['required'] = _("You must accept the Terms of Service")

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if User.objects.filter(username=username).exists():
            raise ValidationError(_("Username already taken."))
        return username

    def clean_password_check(self):
        password1 = self.cleaned_data.get('password')
        print password1
        password2 = self.cleaned_data.get('password_check')
        print password2
        if password1 != password2:
                raise ValidationError(_("passwords do not match."))
        return password1

class ProfileEditForm(forms.Form):
    profile_pic = forms.ImageField(label=_("Profile Picture"),
                                   required=False) #TODO add link to default image (in static folder)
    email = forms.EmailField(label=_("Recovery Email Address"),
                             required=False)
    forename = forms.SlugField(label=_("Forename"),
                               max_length=30,
                               required=False)
    surname = forms.SlugField(label=_("Surname"),
                              max_length=30,
                              required=False)
    bio = forms.CharField(label=_("Bio"),
                          max_length=400,
                          widget=forms.Textarea,
                          required=False)


def sign_up(request):
    if request.method == 'POST': # If the form has been submitted...
        form = SignUpForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data

            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = User.objects.create_user(username = username, password = password)
            user.save()

            profile = Profile(user=user)
            profile.save()

            user = authenticate(username=username, password=password)
            login(request, user)

            return HttpResponseRedirect("/profile/edit") # Redirect after POST
    else:
        form = SignUpForm() # An unbound form

    return render(request, "accounts/sign_up.html", {'form': form})

@login_required
def profile(request):
    profile = Profile.objects.get(user=request.user)
    return render(request, "accounts/profile.html", {'profile': profile})

@login_required
def profile_edit(request):
    if request.method == 'POST': # If the form has been submitted...
        form = ProfileEditForm(request.POST, request.FILES) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            email = form.cleaned_data['email']
            forename = form.cleaned_data['forename']
            surname = form.cleaned_data['surname']
            bio = form.cleaned_data['bio']
            pic = form.cleaned_data['profile_pic']

            user = request.user
            if(user.email != email):
                user.email = email
                user.save()

            profile = Profile.objects.get(user=request.user)

            if pic:
                #get a unique filename
                now = datetime.datetime.now()
                pic_name = hashlib.md5(user.username + now.strftime('%b%d%I%M%p%G')).hexdigest()
                profile.profile_pic.save(pic_name, pic)

            profile.forename = forename
            profile.surname = surname
            profile.bio = bio
            profile.save()


            return HttpResponseRedirect("/profile") # Redirect after POST
    else:
        profile = Profile.objects.get(user=request.user)
        data = {'email':request.user.email, 'forename':profile.forename, 'surname':profile.surname, 'bio':profile.bio, 'profile_pic':profile.profile_pic}
        form = ProfileEditForm(initial=data) # An unbound form
    return render(request, "accounts/profile_edit.html", {'form': form})



@login_required
def accounts_list(request):
    accounts = Account.objects.filter(user=request.user)
    return render(request, "accounts/accounts.html", {"accounts":accounts})

@login_required
def new_account(request):
    return render(request, "accounts/new_account_picker.html")

@login_required
def new_account_auth(request, service_name):
    print service_name

    #in case the previous auth flow was interupted (user presses back, closes window)
    try:
        flow = Flow.objects.get(user=request.user, service=service_name)
        flow.delete()
    except  Flow.DoesNotExist:
        pass

    if service_name == Account.GOOGLE:
        return auth_google(request)
    elif service_name == Account.FACEBOOK:
        return auth_facebook(request)
    #elif service_name == 'otherservice'    
    #	return auth_otherservice(request)

    else: #unknown argument, serve the service picker page
        return HttpResponseRedirect("/accounts/profile/edit")


# This oauth2callback is Google specific; we need a different function for Facebook.
@login_required
def oauth_google(request):

    # Get the user's flow object, and obliterate it
    flow = get_object_or_404(Flow, user=request.user)
    service_name = flow.service
    flow_obj = flow.flow
    flow.delete()
    credentials = flow_obj.step2_exchange(request.GET["code"])

    # Get the user's e-mail address
    response = urllib2.urlopen("https://www.googleapis.com/oauth2/v1/userinfo?access_token="+credentials.access_token)
    json_response = json.loads(response.read())
    email_address = json_response['email']

    account = Account(service=Account.GOOGLE,
                    user=request.user,
                    username=email_address)
    account.save()

    google_account = GoogleAccount(base_account = account,
                                        email_address = email_address,
                                        credentials = credentials)
    google_account.save()
    
    return HttpResponseRedirect("/accounts")

@login_required
def oauth_facebook(request):
    #TODO: authorise facebook
    return HttpResponseRedirect("/accounts")

@login_required
def accounts_list(request):
    accounts = Account.objects.filter(user=request.user)    
    return render(request, "accounts/accounts.html", {"accounts":accounts})

