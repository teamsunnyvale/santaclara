# This script handles low-level e-mail operations.
#
# Copyright (C) 2013 Bilal Akhtar <me@itsbilal.com>

from emails.models import *

import imaplib
import email as EmailModule
import os

# API keys
from emails.api_keys import CLIENT_ID, CLIENT_SECRET

from accounts.models import Account
from messages.models import Sender, Message

# Login and return a logged-in IMAP subclass
def login(emailaccount): 
    imap = imaplib.IMAP4_SSL("imap.googlemail.com")
    
    if emailaccount.service == Account.GOOGLE:
        # Create base auth string for SASL
        auth_string = 'user=%s\1auth=Bearer %s\1\1' % (emailaccount.username,
                                         emailaccount.googleaccount.credentials.access_token) 

        imap.authenticate("XOAUTH2", lambda x: auth_string)

    return imap

def get_text_message_content(message):
    if message.is_multipart():
        for part in message.get_payload():
            if not part.is_multipart():
                return part.get_payload()
        return ""
    else:
        return message.get_payload()

# Find Messages.sender for this e-mail
# If none found, then create one
def determine_email_sender(message):
    from_address = message.get("From")

    try:
        sender = Sender.objects.get(email_address=from_address)
    except Sender.DoesNotExist:
        # TODO: Parse name and address
        sender = Sender(full_name=from_address,
                        email_address=from_address)
        sender.save()

    return sender

def sync_emails_with_db(emailaccount):
    imap = login(emailaccount)

    imap.select('INBOX')
    typ, inbox = imap.search(None, "ALL")
    
    existing_messages = emailaccount.googleaccount.email_set.values_list('messageid',flat=True)

    # TODO: Further optimize this
    for num in inbox[0].split():
        typ, rawmessage = imap.fetch(num, "(RFC822)")

        message = EmailModule.message_from_string(rawmessage[0][1])
        message_id = message.get("Message-ID")

        if message_id in existing_messages:
            continue
        
        # TODO: Fix this, by adding code to determine and match sender
        new_message = Message(sender=determine_email_sender(message),
                        content=get_text_message_content(message).decode("iso-8859-1"),
                        account=emailaccount)
        new_message.save()

        new_email = Email(from_address=message.get("From"),
                        subject=message.get("Subject").decode("iso-8859-1"),
                        messageid=message_id,
                        message=new_message,
                        googleaccount=emailaccount.googleaccount) 
        new_email.save()

    imap.logout()
