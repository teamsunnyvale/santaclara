from django.db import models
from django.contrib.auth.models import User
from oauth2client.django_orm import CredentialsField, FlowField

from main.models import *
from messages.models import Message
from accounts.models import *

# Create your models here.
class Email(models.Model):
    from_address = models.EmailField()
    subject = models.CharField(max_length=100)
    
    # Content moved to common Message model
    #content = models.TextField()

    messageid = models.CharField(max_length=80)
    received = models.DateTimeField(auto_now_add=True)
    message = models.ForeignKey(Message)

    googleaccount = models.ForeignKey(GoogleAccount)

    def __unicode__(self):
        return self.subject


