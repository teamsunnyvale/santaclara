from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from emails.models import *
from emails.Emails import sync_emails_with_db

# Create your views here.

@login_required
def list(request):
    emails = Email.objects.all()

    return render(request, "emails/list.html", {"emails":emails})


# TODO: Fix this view
@login_required
def detail(request, email_id):

    email = get_object_or_404(Email,id=email_id)

    return render(request, "emails/detail.html", {"email":email})

@login_required
def refresh(request):
    # TODO: Refresh all accounts, not just first
    email_accounts = request.user.account_set.filter(service=Account.GOOGLE)

    for email_account in email_accounts:
        sync_emails_with_db(email_account)

    return HttpResponseRedirect(reverse("emails"))
