from django.contrib import admin
from messages.models import *

admin.site.register([Message,Sender,FacebookMessage,DeliveryStatus])
