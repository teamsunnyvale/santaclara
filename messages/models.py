from django.db import models
from accounts.models import Account,Profile

# Relevant for all non-Santaclara registered senders
# If user is registered, then TODO: Implement OneToOne
# field pointing to accounts.profile
class Sender(models.Model):
    full_name = models.CharField(max_length=100)

    # Just fill whichever is important for this sender,
    # so we can do reverse matching
    email_address = models.EmailField(blank=True)
    profile_url = models.URLField(blank=True)
    
    def __unicode__(self):
        return self.full_name# Base for all other message types

class Message(models.Model):
    sender = models.ForeignKey(Sender, related_name = 'sndr+')
    content = models.TextField()
    time_sent = models.DateTimeField(auto_now_add = True)

    account = models.ForeignKey(Account)

    def __unicode__(self):
        return self.content

#provides delivery status of messages on a per-user basis
class DeliveryStatus(models.Model):
    UNKNOWN = 'u' #status is not know, or unsupported by this type of message (eg. email)
    SENT = 's'
    DELIVERED = 'd'
    READ = 'r'
    STATUS_CHOICES = (
            (UNKNOWN, 'unknown'),
            (SENT, 'sent'),
            (DELIVERED, 'delivered'),
            (READ, 'read'),
        )

    user = models.ForeignKey(Profile)
    status = models.CharField(max_length = 1, choices = STATUS_CHOICES, default = UNKNOWN)

    def __unicode__(self):
        return self.status

class FacebookMessage(models.Model):
    # should this field point to Sender instead of Profile? -- bilal
    participants = models.ManyToManyField(Profile, related_name = 'par+')
    delivery_status = models.ManyToManyField(DeliveryStatus)
    message = models.OneToOneField(Message)
