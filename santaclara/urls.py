from django.conf.urls import *
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

# TODO: Smarter generation of these, rather than hardcoded localhost
GOOGLE_AUTH_REDIRECT_URL = "http://localhost:8000/accounts/oauth_google"
FACEBOOK_AUTH_REDIRECT_URL = "http://localhost:8000/accounts/oauth_facebook"

urlpatterns = patterns('main.views',
    url(r'^$', 'index', name='index'),
)

urlpatterns += patterns('accounts.views',
    url(r'^accounts/$', 'accounts_list',name='accounts_list'),
    url(r'^accounts/oauth_google$', 'oauth_google', name='oauth_google'),
    url(r'^accounts/oauth_facebook$', 'oauth_facebook', name='oauth_facebook'),
    url(r'^accounts/add/(?P<service_name>\w{2})','new_account_auth', name='new_account_auth'),
    url(r'^accounts/add/','new_account', name='new_account_picker'),

    url(r'^signup/$', 'sign_up', name='sign_up'),

    url(r'^profile$', 'profile', name='profile'),
    url(r'^profile/edit$', 'profile_edit', name='profile_edit'),
)

urlpatterns += patterns('emails.views',

    # These views are placeholder and will disappear at a later stage
    url(r'^emails/$', 'list', name='emails'),
    url(r'^emails/(?P<email_id>\d+)/', 'detail', name='emails_detail'),
    url(r'^emails/refresh/$', 'refresh', name='emails_refresh'),
)

urlpatterns += patterns('',

    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'main/login.html'}, name="login"),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page':'/'}, name='logout'),
    # url(r'^santaclara/', include('santaclara.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('django.views.static',
        ( r'^%s(?P<path>.*)' % settings.STATIC_URL[1:], 'serve', {'document_root': settings.STATIC_ROOT} ), #for static CSS, Js, etc. Dev only
        ( r'^%s(?P<path>.*)' % settings.MEDIA_URL[1:], 'serve', {'document_root': settings.MEDIA_ROOT} ), #for user-added files. Dev only
    )